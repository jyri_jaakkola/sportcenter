<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentHinnastoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_hinnasto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->float('price')->nullable();
            $table->timestamps();
        });

        Schema::create('child_hinnasto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_hinnasto_id')->unsigned();
            $table->string('type');
            $table->float('price');
            $table->timestamps();

            $table->foreign('parent_hinnasto_id')->references('id')
                  ->on('parent_hinnasto')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parent_hinnasto');
        Schema::drop('child_hinnasto');
    }
}
