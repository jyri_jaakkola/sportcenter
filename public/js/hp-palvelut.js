new Vue({
    el: '#palvelut',

    data: {
        changes: false
    },

    ready: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.$http.get('/json/palvelut.json?' + new Date().getTime(), function(palvelut) {
                this.$set('palvelut', palvelut);
            });
        },
        submitSaveChanges: function() {
            this.$http.post('/api/hp-save-palvelut', JSON.stringify(this.palvelut));
            this.changes = false;
        }
    }
});
