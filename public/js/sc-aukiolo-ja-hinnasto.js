Vue.component('sc-edit-aukioloaika', {
    template: '#edit-aukioloaika',
    props: ['array', 'aukioloaika'],
    data: function() {
        return {
            showEdit: false,
            weekdays: [
                'Maanantai',
                'Tiistai',
                'Keskiviikko',
                'Torstai',
                'Perjantai',
                'Lauantai',
                'Sunnuntai'
            ],
            newAukiolo: {}
        };
    },
    methods: {
        toggleEdit: function() {
            this.showEdit = ! this.showEdit;
        },
        onSubmitEdit: function(e) {
            e.preventDefault();
            var aukiolo = this.newAukiolo;
            aukiolo.id = this.aukioloaika.id;
            this.$http.patch('api/sc-update-aukiolo/'+ aukiolo.id, aukiolo)
            .then(function (response) {
                this.aukioloaika = response.data
            }, function (error) {});
            this.toggleEdit();
        },
        delete: function(e) {
            e.preventDefault();
            if (confirm("Haluatko varmasti poistaa aukioloajan?")) {
                this.$http.delete('api/sc-delete-aukiolo/'+ this.aukioloaika.id);
                this.array.$remove(this.aukioloaika);
            }
        }
    }
});

Vue.component('sc-add-aukioloaika', {
    template: '#add-aukioloaika',
    props: ['array'],
    data: function() {
        return {
            showAdd: false,
            weekdays: [
                'Maanantai',
                'Tiistai',
                'Keskiviikko',
                'Torstai',
                'Perjantai',
                'Lauantai',
                'Sunnuntai'
            ],
            newAukiolo: {}
        };
    },
    methods: {
        toggleAdd: function() {
            this.showAdd = ! this.showAdd;
        },
        onSubmitAdd: function(e) {
            e.preventDefault();
            var aukiolo = this.newAukiolo;
            this.$http.post('api/sc-add-aukiolo', aukiolo)
            .then(function (response) {
                this.array.push(response.data);
            }, function (error) {});
            this.newAukiolo = {};
            this.toggleAdd();
        }
    }
});

Vue.component('sc-edit-hinnasto', {
    template: '#edit-hinnasto',
    props: ['array', 'hinnasto'],
    data: function() {
        return {
            showEdit: false,
            newHinnasto: {}
        };
    },
    methods: {
        toggleEdit: function() {
            this.showEdit = ! this.showEdit;
        },
        onSubmitEdit: function(e) {
            e.preventDefault();
            var hinnasto = this.newHinnasto;
            hinnasto.id = this.hinnasto.id;
            this.$http.patch('api/sc-update-hinnasto/'+ hinnasto.id, hinnasto)
            .then(function (response) {
                this.hinnasto = response.data
            }, function (error) {});
            this.toggleEdit();
        },
        delete: function(e) {
            e.preventDefault();
            if (confirm("Haluatko varmasti poistaa tämän hinnan?")) {
                this.$http.delete('api/sc-delete-hinnasto/'+ this.hinnasto.id);
                this.array.$remove(this.hinnasto);
            }
        }
    }
});

Vue.component('sc-add-hinnasto', {
    template: '#add-hinnasto',
    props: ['array'],
    data: function() {
        return {
            showAdd: false,
            newHinnasto: {}
        };
    },
    methods: {
        toggleAdd: function() {
            this.showAdd = ! this.showAdd;
        },
        onSubmitAdd: function(e) {
            e.preventDefault();
            var hinnasto = this.newHinnasto;
            this.$http.post('api/sc-add-hinnasto', hinnasto)
            .then(function (response) {
                this.array.push(response.data);
            }, function (error) {});
            this.newHinnasto = {};
            this.toggleAdd();
        }
    }
});


new Vue({
    el: '#aukioloajat-ja-hinnasto',

    data: {
        weekdays: [
            'Maanantai',
            'Tiistai',
            'Keskiviikko',
            'Torstai',
            'Perjantai',
            'Lauantai',
            'Sunnuntai'
        ],
        changes: false
    },

    ready: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.$http.get('/json/aukiolo-ja-hinnasto.json?' + new Date().getTime(), function(aukiolo) {
                this.$set('aukiolo', aukiolo);
            });
            this.$http.get('/api/sc-aukioloajat', function(aukioloajat) {
                this.$set('aukioloajat', aukioloajat);
            });
            this.$http.get('/api/sc-hinnasto', function(hinnasto) {
                this.$set('hinnasto', hinnasto);
            });
        },
        submitSaveChanges: function() {
            this.$http.post('/api/sc-save-aukiolo', JSON.stringify(this.aukiolo));
            this.changes = false;
        }

    }

});
