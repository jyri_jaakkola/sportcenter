new Vue({
    el: '#etusivu',

    data: {
        changes: false
    },

    ready: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.$http.get('/json/hierontapalvelu.json?' + new Date().getTime(), function(etusivu) {
                this.$set('etusivu', etusivu);
            });
        },
        submitSaveChanges: function() {
            this.$http.post('/api/hp-save-etusivu', JSON.stringify(this.etusivu));
            this.changes = false;
        }
    }
});
