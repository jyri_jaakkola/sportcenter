Vue.http.headers.common['X-CSRF-TOKEN'] = $('#token').attr('value');

Vue.component('sc-add-text', {
    template: '#add-text',

    props: ['data', 'changes'],

    data: function() {
        return {
            showHeading: false,
            showParagraph: false,
            newHeading: '',
            newParagraph: ''
        };
    },

    methods: {
        toggleHeading: function() {
            this.showHeading = ! this.showHeading;
            this.showParagraph = false;
        },
        toggleParagraph: function() {
            this.showParagraph = ! this.showParagraph;
            this.showHeading = false;
        },
        onSubmitNewHeading: function(e) {
            e.preventDefault();
            this.data.push({"type": "heading", "text": this.newHeading});
            this.changes = true;
            this.toggleHeading();
            this.newHeading = '';
        },
        onSubmitNewParagraph: function(e) {
            e.preventDefault();
            this.data.push({"type": "paragraph", "text": this.newParagraph});
            this.changes = true;
            this.toggleParagraph();
            this.newParagraph = '';
        }
    }
});

Vue.component('sc-edit-text', {
    template: '#edit-text',

    props: ['array', 'data', 'changes'],

    data: function() {
        return {
            showEdit: false,
            newText: ''
        };
    },

    ready: function() {
        this.newText = this.data.text;
    },

    methods: {
        toggleEdit: function() {
            this.showEdit = ! this.showEdit;
        },
        onSubmitEdit: function(e) {
            e.preventDefault();
            this.data.text = this.newText;
            this.changes = true;
            this.toggleEdit();
        },
        delete: function(e) {
            e.preventDefault();
            if(confirm('Haluatko varmasti poistaa elementin "'+
                        this.data.text +'"?')) {
                this.array.$remove(this.data);
                this.changes = true;
            }
        }
    }
});
