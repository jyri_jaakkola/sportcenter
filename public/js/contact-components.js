Vue.component('sc-add-contact', {
    template: '#add-contact',

    props: ['data', 'changes'],

    data: function() {
        return {
            showAdd: false,
            newName: '',
            newPhone: ''
        };
    },

    methods: {
        toggleAdd: function() {
            this.showAdd = ! this.showAdd;
        },
        onSubmitNewContact: function(e) {
            e.preventDefault();
            this.data.push({"name": this.newName, "phone": this.newPhone});
            this.changes = true;
            this.toggleAdd();
            this.newName = '';
            this.newPhone = '';
        }
    }
});

Vue.component('sc-edit-contact', {
    template: '#edit-contact',

    props: ['array', 'data', 'changes'],

    data: function() {
        return {
            showEdit: false,
            newName: '',
            newPhone: ''
        };
    },

    ready: function() {
        this.newName = this.data.name;
        this.newPhone = this.data.phone;
    },

    methods: {
        toggleEdit: function() {
            this.showEdit = ! this.showEdit;
        },
        onSubmitEditContact: function(e) {
            e.preventDefault();
            this.data.name = this.newName;
            this.data.phone = this.newPhone;
            this.changes = true;
            this.toggleEdit();
        },
        delete: function(e) {
            e.preventDefault();
            if(confirm('Haluatko varmasti poistaa yhteystiedon "'+
                        this.data.name +'"?')) {
                this.array.$remove(this.data);
                this.changes = true;
            }
        }
    }
});
