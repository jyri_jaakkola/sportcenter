new Vue({
    el: '#yhteystiedot',

    data: {
        changes: false
    },

    ready: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.$http.get('/json/yhteystiedot.json?' + new Date().getTime(), function(yhteystiedot) {
                this.$set('yhteystiedot', yhteystiedot);
            });
        },
        submitSaveChanges: function() {
            this.$http.post('/api/sc-save-yhteystiedot', JSON.stringify(this.yhteystiedot));
            this.changes = false;
        }
    }
});
