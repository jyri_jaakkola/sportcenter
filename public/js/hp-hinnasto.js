Vue.component('hp-edit-hinnasto', {
    template: '#edit-hinnasto',
    props: ['array', 'hinnasto', 'type'],
    data: function() {
        return {
            showEdit: false,
            newHinnasto: {}
        };
    },
    methods: {
        toggleEdit: function() {
            this.showEdit = ! this.showEdit;
        },
        onSubmitEdit: function(e) {
            e.preventDefault();
            var hinnasto = this.newHinnasto;
            if (this.type == "parent") {
                this.$http.patch('api/hp-update-parent/'+ this.hinnasto.id, hinnasto)
                .then(function (response) {
                    this.hinnasto = response.data
                }, function (error) {});
            } else {
                this.$http.patch('api/hp-update-child/'+ this.hinnasto.id, hinnasto)
                .then(function (response) {
                    this.hinnasto = response.data
                }, function (error) {});
            }
            this.toggleEdit();
        },
        delete: function(e) {
            e.preventDefault();
            if (confirm("Haluatko varmasti poistaa tämän hinnan?")) {
                if (this.type == "parent") {
                    this.$http.delete('api/hp-delete-parent/'+ this.hinnasto.id);
                    this.array.$remove(this.hinnasto);
                } else {
                    this.$http.delete('api/hp-delete-child/'+ this.hinnasto.id);
                    this.array.childs.$remove(this.hinnasto);
                }

            }
        }
    }
});

Vue.component('hp-add-hinnasto', {
    template: '#add-hinnasto',
    props: ['hinnasto'],
    data: function() {
        return {
            showAddParent: false,
            showAddChild: false,
            newHinnasto: {},
            parentId: 1
        };
    },
    methods: {
        toggleAddParent: function() {
            this.showAddParent = ! this.showAddParent;
            this.showAddChild = false;
        },
        toggleAddChild: function() {
            this.showAddChild = ! this.showAddChild;
            this.showAddParent = false;
        },
        onSubmitAddParent: function(e) {
            e.preventDefault();
            var hinnasto = this.newHinnasto;
            this.$http.post('api/hp-add-hinnasto', hinnasto)
            .then(function (response) {
                 this.hinnasto.push(response.data);
                 this.newHinnasto = {};
            }, function (error) {});
            this.toggleAddParent();
        },
        onSubmitAddChild: function(e) {
            e.preventDefault();
            var hinnasto = this.newHinnasto;
            this.$http.post('api/hp-add-hinnasto/' + this.parentId, hinnasto)
            .then(function (response) {
                 for (var i = 0; i<this.hinnasto.length; i++) {
                     if (this.hinnasto[i].id == this.parentId) {
                         this.hinnasto[i].childs.push(response.data);
                         this.newHinnasto = {};
                     }
                 }
            }, function (error) {});
            this.toggleAddChild();
        }
    }
});

new Vue({
    el: '#hinnasto',

    data: {
        changes: false
    },

    ready: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.$http.get('/json/hinnasto.json?' + new Date().getTime(), function(texts) {
                this.$set('texts', texts);
            });

            this.$http.get('/api/hp-hinnasto', function(hinnasto) {
                this.$set('hinnasto', hinnasto);
            });
        },
        submitSaveChanges: function() {
            this.$http.post('/api/hp-save-hinnasto', JSON.stringify(this.texts));
            this.changes = false;
        }

    }

});
