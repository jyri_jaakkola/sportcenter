<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
?>

<template id="edit-aukioloaika">
    <div class="edit-symbol">
        <div class="edit pull-left" @click="toggleEdit">
            &#x270D;
        </div>
        <div class="remove pull-left" @click="delete">
            &#x2718;
        </div>
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showEdit">
        <form method="POST" @submit="onSubmitEdit">
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="day">Päivä:</label>
                <select class="form-control" name="day" v-model="newAukiolo.day">
                    <option v-for="weekday in weekdays"
                            value="@{{ $index+1 }}"
                            selected="@{{ $index+1 == aukioloaika.day }}"
                    >
                        @{{ weekday }}
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="from">Alkaen:</label>
                <input type="time"
                       name="from"
                       class="form-control"
                       value="@{{ aukioloaika.from }}"
                       v-model="newAukiolo.from">
            </div>
            <div class="form-group">
                <label for="to">Päättyen:</label>
                <input type="time"
                       name="to"
                       class="form-control"
                       value="@{{ aukioloaika.to }}"
                       v-model="newAukiolo.to">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Muokkaa
                </button>
            </div>
        </form>
    </div>
</template>

<template id="add-aukioloaika">
    <div class="edit-symbol add {{ $alltwelve }}" @click="toggleAdd">
        &#x2295;
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showAdd">
        <form method="POST" @submit="onSubmitAdd">
            <div class="form-group">
                <label for="day">Päivä:</label>
                <select class="form-control" name="day" v-model="newAukiolo.day">
                    <option v-for="weekday in weekdays"
                            value="@{{ $index+1 }}"
                    >
                        @{{ weekday }}
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="from">Alkaen:</label>
                <input type="time"
                       name="from"
                       class="form-control"
                       v-model="newAukiolo.from">
            </div>
            <div class="form-group">
                <label for="to">Päättyen:</label>
                <input type="time"
                       name="to"
                       class="form-control"
                       v-model="newAukiolo.to">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Lisää
                </button>
            </div>
        </form>
    </div>
</template>
