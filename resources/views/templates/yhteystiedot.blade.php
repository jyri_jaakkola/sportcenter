<template id="add-contact">
    <button type="button" class="btn btn-default" @click="toggleAdd">
        Uusi yhteystieto
    </button>

    <form method="POST" v-show="showAdd" @submit="onSubmitNewContact">
        <div class="form-group">
            <label for="name">Nimi</label>
            <input type="text" name="name" class="form-control" v-model="newName">
        </div>
        <div class="form-group">
            <label for="phone">Puhelin</label>
            <input type="tel" name="phone" class="form-control" v-model="newPhone">
        </div>
        <div class="form-group">
            <button type="submit" class="form-control btn btn-success">
                Lisää
            </button>
        </div>
    </form>
</template>

<template id="edit-contact">
    <div class="edit-symbol">
        <div class="edit pull-left" @click="toggleEdit">
            &#x270D;
        </div>
        <div class="remove pull-left" @click="delete">
            &#x2718;
        </div>
    </div>

    <form method="POST" v-show="showEdit" @submit="onSubmitEditContact">
        <div class="form-group">
            <label for="name">Nimi</label>
            <input type="text" name="name" class="form-control" v-model="newName">
        </div>
        <div class="form-group">
            <label for="phone">Puhelin</label>
            <input type="tel" name="phone" class="form-control" v-model="newPhone">
        </div>
        <div class="form-group">
            <button type="submit" class="form-control btn btn-success">
                Muokkaa
            </button>
        </div>
    </form>
</template>
