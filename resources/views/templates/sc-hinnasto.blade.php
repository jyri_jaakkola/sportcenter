<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
?>

<template id="edit-hinnasto">
    <div class="edit-symbol">
        <div class="edit pull-left" @click="toggleEdit">
            &#x270D;
        </div>
        <div class="remove pull-left" @click="delete">
            &#x2718;
        </div>
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showEdit">
        <form method="POST" @submit="onSubmitEdit">
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="type">Tyyppi:</label>
                <input type="text"
                       name="type"
                       class="form-control"
                       value="@{{ hinnasto.type }}"
                       v-model="newHinnasto.type">
            </div>
            <div class="form-group">
                <label for="normal">Normaali:</label>
                <input type="number"
                       step="0.05"
                       name="normal"
                       class="form-control"
                       value="@{{ hinnasto.normal }}"
                       v-model="newHinnasto.normal">
            </div>
            <div class="form-group">
                <label for="student">Opiskelija:</label>
                <input type="number"
                       step="0.05"
                       name="to"
                       class="form-control"
                       value="@{{ hinnasto.student }}"
                       v-model="newHinnasto.student">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Muokkaa
                </button>
            </div>
        </form>
    </div>
</template>

<template id="add-hinnasto">
    <div class="edit-symbol add {{ $alltwelve }}" @click="toggleAdd">
        &#x2295;
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showAdd">
        <form method="POST" @submit="onSubmitAdd">
            <div class="form-group">
                <label for="type">Tyyppi:</label>
                <input type="text"
                       name="type"
                       class="form-control"
                       v-model="newHinnasto.type">
            </div>
            <div class="form-group">
                <label for="normal">Normaali:</label>
                <input type="number"
                       step="0.05"
                       name="normal"
                       class="form-control"
                       v-model="newHinnasto.normal">
            </div>
            <div class="form-group">
                <label for="student">Opiskelija:</label>
                <input type="number"
                       step="0.05"
                       name="to"
                       class="form-control"
                       v-model="newHinnasto.student">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Lisää
                </button>
            </div>
        </form>
    </div>
</template>
