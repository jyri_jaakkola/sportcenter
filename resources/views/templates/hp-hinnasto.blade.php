<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
?>

<template id="edit-hinnasto">
    <div class="edit-symbol">
        <div class="edit pull-left" @click="toggleEdit">
            &#x270D;
        </div>
        <div class="remove pull-left" @click="delete">
            &#x2718;
        </div>
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showEdit">
        <form method="POST" @submit="onSubmitEdit">
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="type">Hoito:</label>
                <input type="text"
                       name="type"
                       class="form-control"
                       value="@{{ hinnasto.type }}"
                       v-model="newHinnasto.type">
            </div>
            <div class="form-group">
                <label for="price">Hinta:</label>
                <input type="number"
                       step="0.05"
                       name="price"
                       class="form-control"
                       value="@{{ hinnasto.price }}"
                       v-model="newHinnasto.price">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Muokkaa
                </button>
            </div>
        </form>
    </div>
</template>

<template id="add-hinnasto">
    <div class="{{ $alltwelve }}">
        <button type="button" class="btn btn-default" @click="toggleAddParent">
            Uusi päätaso
        </button>
        <button type="button" class="btn btn-default" @click="toggleAddChild">
            Uusi alataso
        </button>
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showAddParent">
        <form method="POST" @submit="onSubmitAddParent">
            <div class="form-group">
                <label for="type">Hoitomuoto:</label>
                <input type="text"
                       name="type"
                       class="form-control"
                       v-model="newHinnasto.type">
            </div>
            <div class="form-group">
                <label for="price">Hinta (voi jättää tyhjäksi):</label>
                <input type="number"
                       step="0.05"
                       name="price"
                       class="form-control"
                       v-model="newHinnasto.price">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Lisää
                </button>
            </div>
        </form>
    </div>
    <div class="edit-form {{ $alltwelve }}" v-show="showAddChild">
        <form method="POST" @submit="onSubmitAddChild">
            <div class="form-group">
                <label for="parent">Hoitomuoto:</label>
                <select class="form-control" name="parent" v-model="parentId">
                    <option v-for="parent in hinnasto" value="@{{ parent.id }}">
                        @{{ parent.type }}
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="type">Hoito:</label>
                <input type="text"
                       name="type"
                       class="form-control"
                       v-model="newHinnasto.type">
            </div>
            <div class="form-group">
                <label for="price">Hinta:</label>
                <input type="number"
                       step="0.05"
                       name="price"
                       class="form-control"
                       v-model="newHinnasto.price">
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">
                    Lisää
                </button>
            </div>
        </form>
    </div>
</template>
