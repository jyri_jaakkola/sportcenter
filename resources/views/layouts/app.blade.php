<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="css/vendor.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
</head>

<body id="app-layout">
    <div class="wrapper">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        @if ($_SERVER['HTTP_HOST'] == "sportcenter.jaakkola.dy.fi")
                            Kokemäen Sport Center
                        @else
                            Hierontapalvelu Markku Jaakkola
                        @endif
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">Etusivu</a></li>

                        @if ($_SERVER['HTTP_HOST'] == "sportcenter.jaakkola.dy.fi")
                            <li><a href="{{ url('/aukioloajat-ja-hinnasto') }}">Aukioloajat ja hinnasto</a></li>
                            <li><a href="http://hierontapalvelu.jaakkola.dy.fi">Hierontapalvelu Markku Jaakkola Tmi</a></li>
                            <li><a href="{{ url('/yhteystiedot') }}">Yhteystiedot</a></li>
                        @else
                            <li><a href="{{ url('/palvelut') }}">Palvelut</a></li>
                            <li><a href="{{ url('/hinnasto') }}">Hinnasto</a></li>
                            <li><a href="http://sportcenter.jaakkola.dy.fi">Kokemäen Sport Center Oy</a></li>
                            <li><a href="{{ url('/yhteystiedot') }}">Yhteystiedot</a></li>
                        @endif

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/profiili') }}">Profiili</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Kirjaudu ulos</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        @include('layouts.text-templates')


        <div class="push"></div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-center text-muted">{{ date('Y') }} &copy; Jyri Jaakkola</p>
        </div>
    </footer>


    <!-- JavaScripts -->
    <script src="/js/vendor.js"></script>
    <script src="/js/public-components.js"></script>
    @yield('footer')
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
