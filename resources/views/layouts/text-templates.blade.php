<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
?>

<template id="add-text">
    <div class="{{ $alltwelve }}">
        <button type="button" class="btn btn-default" @click="toggleHeading">
            Uusi otsikko
        </button>
        <button type="button" class="btn btn-default" @click="toggleParagraph">
            Uusi kappale
        </button>
    </div>

    <form class="{{ $alltwelve }}" method="POST" v-show="showHeading" @submit="onSubmitNewHeading">
        <div class="form-group">
            <label for="heading">Uusi otsikko</label>
            <input type="text" name="heading" class="form-control" v-model="newHeading">
        </div>
        <div class="form-group">
            <button type="submit" class="form-control btn btn-success">
                Lisää
            </button>
        </div>
    </form>

    <form class="{{ $alltwelve }}" method="POST" v-show="showParagraph" @submit="onSubmitNewParagraph">
        <div class="form-group">
            <label for="paragraph">Uusi kappale</label>
            <textarea name="paragraph" class="form-control" v-model="newParagraph">
            </textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="form-control btn btn-success">
                Lisää
            </button>
        </div>
    </form>
</template>

<template id="edit-text">
    <div class="edit-symbol">
        <div class="edit pull-left" @click="toggleEdit">
            &#x270D;
        </div>
        <div class="remove pull-left" @click="delete">
            &#x2718;
        </div>
    </div>

    <form class="{{ $alltwelve }}" method="POST" v-show="showEdit" @submit="onSubmitEdit">
        <div class="form-group" v-if="this.data.type == 'heading'">
            <label for="heading">Otsikko</label>
            <input type="text"
                   name="heading"
                   class="form-control"
                   v-model="newText">
        </div>
        <div class="form-group" v-else>
            <label for="paragraph">Kappale</label>
            <textarea name="paragraph" class="form-control" v-model="newText">
            </textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="form-control btn btn-success">
                Muokkaa
            </button>
        </div>
    </form>
</template>
