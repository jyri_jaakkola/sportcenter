@extends('layouts.app')

@section('meta')
    @if ( Auth::check() )
        <meta id="token" name="token" value="{{ csrf_token() }}">
    @endif
@endsection

@section('title')
Hierontapalvelu Markku Jaakkola - Palvelut
@endsection

<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
    $allten = "col-md-10 col-sm-10 col-xs-10";
    $alltwo = "col-md-2 col-sm-2 col-xs-2";
?>

@section('content')
<div class="container" id="palvelut">
    <div class="row {{ $alltwelve }}">
        @if ( Auth::check() )
            <button class="btn btn-danger pull-right"
                    v-show="changes"
                    @click="submitSaveChanges"
            >
                Tallenna muutokset
            </button>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}">
            <div class="{{ $alltwelve }}" v-for="object in palvelut">
                <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                    @{{ object.text }}
                </h1>
                <p class="{{ $allten }}" v-else>
                    @{{ object.text }}
                </p>
                @if ( Auth::check() )
                    <sc-edit-text class="{{ $alltwo }}"
                                  :array.sync=palvelut
                                  :data.sync=object
                                  :changes.sync=changes>
                    </sc-edit-text>
                @endif
            </div>
            @if ( Auth::check() )
                <sc-add-text class="{{ $alltwelve }}"
                             :data.sync=palvelut
                             :changes.sync=changes>
                </sc-add-text>
            @endif
        </div>
    </div>

</div>
@endsection

@section('footer')
    <script src="/js/hp-palvelut.js"></script>
@endsection
