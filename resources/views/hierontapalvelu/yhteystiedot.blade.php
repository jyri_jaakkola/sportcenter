@extends('layouts.app')

@section('meta')
    @if ( Auth::check() )
        <meta id="token" name="token" value="{{ csrf_token() }}">
    @endif
@endsection

@section('title')
Hierontapalvelu Markku Jaakkola - Yhteystiedot
@endsection

<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
    $allten = "col-md-10 col-sm-10 col-xs-10";
    $allsix = "col-md-6 col-sm-6 col-xs-6";
    $allfour = "col-md-4 col-sm-4 col-xs-4";
    $alltwo = "col-md-2 col-sm-2 col-xs-2";
?>

@section('content')
<div class="container" id="yhteystiedot">
    <div class="row {{ $alltwelve }}">
        @if ( Auth::check() )
            <button class="btn btn-danger pull-right"
                    v-show="changes"
                    @click="submitSaveChanges"
            >
                Tallenna muutokset
            </button>
        @endif
    </div>
    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in yhteystiedot.header">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=yhteystiedot.header
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=yhteystiedot.header
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>

    <div class="row {{ $alltwelve }} contacts">
        <div class="{{ $alltwelve }}" v-for="contact in yhteystiedot.contacts">
            <p class="{{ $allfour }} text-right">@{{ contact.name }}</p>
            <p class="{{ $allsix }}">@{{ contact.phone }}</p>
            @if ( Auth::check() )
                <sc-edit-contact class="{{ $alltwo }}"
                              :array.sync=yhteystiedot.contacts
                              :data.sync=contact
                              :changes.sync=changes>
                </sc-edit-contact>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-contact class="{{ $alltwelve }}" :data.sync=yhteystiedot.contacts :changes.sync=changes>
            </sc-add-contact>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in yhteystiedot.footer">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=yhteystiedot.footer
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=yhteystiedot.footer
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>
</div>

@include('templates.yhteystiedot')

@endsection

@section('footer')
    <script src="/js/contact-components.js"></script>
    <script src="/js/hp-yhteystiedot.js"></script>
@endsection
