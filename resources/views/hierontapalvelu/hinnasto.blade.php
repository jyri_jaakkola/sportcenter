@extends('layouts.app')

@section('meta')
    @if ( Auth::check() )
        <meta id="token" name="token" value="{{ csrf_token() }}">
    @endif
@endsection

@section('title')
Hierontapalvelu Markku Jaakkola - Hinnasto
@endsection

<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
    $allten = "col-md-10 col-sm-10 col-xs-10";
    $allsix = "col-md-6 col-sm-6 col-xs-6";
    $allthree = "col-md-3 col-sm-3 col-xs-3";
    $alltwo = "col-md-2 col-sm-2 col-xs-2";
?>

@section('content')
<div class="container" id="hinnasto">
    <div class="row">
        @if ( Auth::check() )
            <button class="btn btn-danger pull-right"
                    v-show="changes"
                    @click="submitSaveChanges"
            >
                Tallenna muutokset
            </button>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in texts.header">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=texts.header
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=texts.header
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}">
            <div class="hinnasto" v-for="parent in hinnasto">
                <div class="parent {{ $alltwelve }}">
                    <div class="{{ $allsix }} top-padded">
                        <strong>@{{ parent.type }}</strong>
                    </div>
                    <div class="{{ $allthree }} text-right" v-if="parent.price != null">
                        <strong>@{{ parent.price }} &euro;</strong>
                    </div>
                    <div class="{{ $allthree}}" v-else></div>

                    @if (Auth::check())
                        <hp-edit-hinnasto class="{{ $alltwo }}"
                                          :array.sync=hinnasto
                                          :hinnasto.sync=parent
                                          type="parent">
                        </hp-edit-hinnasto>
                    @endif
                </div>
                <div class="{{ $alltwelve }}" v-for="child in parent.childs | orderBy 'price'">
                    <div class="{{ $allsix }}">
                        @{{ child.type }}
                    </div>
                    <div class="{{ $allthree }} text-right">
                        @{{ child.price }} &euro;
                    </div>
                    @if (Auth::check())
                        <hp-edit-hinnasto class="{{ $alltwo }}"
                                          :array.sync=parent
                                          :hinnasto.sync=child
                                          type="child">
                        </hp-edit-hinnasto>
                    @endif
                </div>
                <hr class="{{ $allten }}">
            </div>
            @if (Auth::check())
                <hp-add-hinnasto :hinnasto.sync=hinnasto></hp-add-hinnasto>
            @endif
        </div>
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in texts.footer">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text :array.sync=texts.footer
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=texts.footer
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>

</div>

@include('templates.hp-hinnasto')

@endsection

@section('footer')
    <script src="/js/hp-hinnasto.js"></script>
@endsection
