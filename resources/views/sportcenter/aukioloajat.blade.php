@extends('layouts.app')

@section('meta')
    @if ( Auth::check() )
        <meta id="token" name="token" value="{{ csrf_token() }}">
    @endif
@endsection

@section('title')
Kokemäen Sport Center Oy - Aukioloajat ja hinnasto
@endsection

<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
    $allten = "col-md-10 col-sm-10 col-xs-10";
    $allseven = "col-md-7 col-sm-7 col-xs-7";
    $fiveseventwelve = "col-md-5 col-sm-7 col-xs-12";
    $allfive = "col-md-5 col-sm-5 col-xs-5";
    $allfour = "col-md-4 col-sm-4 col-xs-4";
    $allthree = "col-md-3 col-sm-3 col-xs-3";
    $alltwo = "col-md-2 col-sm-2 col-xs-2";
    $allone = "col-md-1 col-sm-1 col-xs-1";
?>

@section('content')
<div class="container" id="aukioloajat-ja-hinnasto">
    <div class="row {{ $alltwelve }}">
        @if ( Auth::check() )
            <button class="btn btn-danger pull-right"
                    v-show="changes"
                    @click="submitSaveChanges"
            >
                Tallenna muutokset
            </button>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in aukiolo.header">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=aukiolo.header
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=aukiolo.header
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $fiveseventwelve }}">
            <h1>Aukioloajat</h1>

            <div v-for="aukiolo in aukioloajat | orderBy 'day'">
                <div class="aukiolo">
                    <div class="{{ $allfive }}">
                        @{{ weekdays[aukiolo.day-1] }}
                    </div>
                    <div class="{{ $alltwo }}">
                        @{{ aukiolo.from }}
                    </div>
                    <div class="{{ $allone }} text-center">-</div>
                    <div class="{{ $alltwo }} text-right">
                        @{{ aukiolo.to }}
                    </div>
                </div>
                @if (Auth::check())
                    <sc-edit-aukioloaika class="{{ $alltwo }}"
                                         :array.sync=aukioloajat
                                         :aukioloaika.sync=aukiolo>
                    </sc-edit-aukioloaika>
                @endif
            </div>
            @if (Auth::check())
                <sc-add-aukioloaika class="{{ $alltwelve }}"
                                    :array.sync=aukioloajat>
                </sc-add-aukioloaika>
            @endif

            <div class="{{ $alltwelve }}">
                <div class="{{ $alltwelve }}" v-for="object in aukiolo.openhours">
                    <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                        @{{ object.text }}
                    </h1>
                    <p class="{{ $allten }}" v-else>
                        @{{ object.text }}
                    </p>
                    @if ( Auth::check() )
                        <sc-edit-text class="{{ $alltwo }}"
                                      :array.sync=aukiolo.openhours
                                      :data.sync=object
                                      :changes.sync=changes>
                        </sc-edit-text>
                    @endif
                </div>
                @if ( Auth::check() )
                    <sc-add-text class="{{ $alltwelve }}"
                                 :data.sync=aukiolo.openhours
                                 :changes.sync=changes>
                    </sc-add-text>
                @endif
            </div>
        </div>



        <div class="{{ $fiveseventwelve }} col-md-offset-2">
            <h1>Hinnasto</h1>

            <div class="{{ $allseven }}"></div>
            <div class="{{ $allthree }} text-right">opiskelija</div>
            <div v-for="hinta in hinnasto | orderBy 'normal'">
                <div class="hinnasto">
                    <div class="{{ $allfour }}">
                        @{{ hinta.type }}
                    </div>
                    <div class="{{ $allthree }} text-right">
                        @{{ hinta.normal }} &euro;
                    </div>
                    <div class="{{ $allthree }} text-right">
                        @{{ hinta.student }} &euro;
                    </div>
                </div>
                @if (Auth::check())
                    <sc-edit-hinnasto class="{{ $alltwo }}"
                                      :array.sync=hinnasto
                                      :hinnasto.sync=hinta>
                    </sc-edit-hinnasto>
                @endif
            </div>
            @if (Auth::check())
                <sc-add-hinnasto class="{{ $alltwelve }}"
                                 :array.sync=hinnasto>
                </sc-add-hinnasto>
            @endif

            <div class="{{ $alltwelve }}">
                <div class="{{ $alltwelve }}" v-for="object in aukiolo.pricelist">
                    <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                        @{{ object.text }}
                    </h1>
                    <p class="{{ $allten }}" v-else>
                        @{{ object.text }}
                    </p>
                    @if ( Auth::check() )
                        <sc-edit-text class="{{ $alltwo }}"
                                      :array.sync=aukiolo.pricelist
                                      :data.sync=object
                                      :changes.sync=changes>
                        </sc-edit-text>
                    @endif
                </div>
                @if ( Auth::check() )
                    <sc-add-text class="{{ $alltwelve }}"
                                 :data.sync=aukiolo.pricelist
                                 :changes.sync=changes>
                    </sc-add-text>
                @endif
            </div>
        </div>
    </div>

    <div class="row {{ $alltwelve }}">
        <div class="{{ $alltwelve }}" v-for="object in aukiolo.footer">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=aukiolo.footer
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=aukiolo.footer
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>
</div>

@include('templates.aukioloajat')
@include('templates.sc-hinnasto')

@endsection

@section('footer')
    <script src="/js/sc-aukiolo-ja-hinnasto.js"></script>
@endsection
