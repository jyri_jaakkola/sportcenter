@extends('layouts.app')

@section('meta')
    @if ( Auth::check() )
        <meta id="token" name="token" value="{{ csrf_token() }}">
    @endif
@endsection

@section('title')
Kokemäen Sport Center Oy
@endsection

<?php
    $alltwelve = "col-md-12 col-sm-12 col-xs-12";
    $elevenoffset = "col-md-11 col-md-offset-1 col-sm-12 col-xs-12";
    $allten = "col-md-10 col-sm-10 col-xs-10";
    $eightoffset = "col-md-8 col-md-offset-1 col-sm-12 col-xs-12";
    $seventwelve = "col-md-7 col-sm-12 col-xs-12";
    $fourtwelve = "col-md-4 col-sm-12 col-xs-12";
    $alltwo = "col-md-2 col-sm-2 col-xs-2";
?>

@section('content')
<div class="container" id="etusivu">
    <div class="row {{ $alltwelve }}">
        @if ( Auth::check() )
            <button class="btn btn-danger pull-right"
                    v-show="changes"
                    @click="submitSaveChanges"
            >
                Tallenna muutokset
            </button>
        @endif
    </div>

    <div class="row {{ $alltwelve }}">
        <img src="sc_logo.png" class="logo" alt="Kokemäen Sport Center" />
    </div>
    <div class="row {{ $elevenoffset }}">
        <div class="{{ $seventwelve }}">
            <div class="{{ $alltwelve }}">
                <div class="{{ $alltwelve }}" v-for="object in etusivu.header">
                    <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                        @{{ object.text }}
                    </h1>
                    <p class="{{ $allten }}" v-else>
                        @{{ object.text }}
                    </p>
                    @if ( Auth::check() )
                        <sc-edit-text class="{{ $alltwo }}"
                                      :array.sync=etusivu.header
                                      :data.sync=object
                                      :changes.sync=changes>
                        </sc-edit-text>
                    @endif
                </div>
                @if ( Auth::check() )
                    <sc-add-text class="{{ $alltwelve }}"
                                 :data.sync=etusivu.header
                                 :changes.sync=changes>
                    </sc-add-text>
                @endif
            </div>
            <img src="kuva01.jpg" alt="kuva" class="img-responsive" />
        </div>
        <p class="{{ $fourtwelve }}">
            <div class="fb-page"
                 data-href="https://www.facebook.com/Kokem&#xe4;en-Sport-Center-Oy-1964484877109333/"
                 data-tabs="timeline"
                 data-small-header="false"
                 data-adapt-container-width="true"
                 data-hide-cover="false"
                 data-show-facepile="true">
            </div>
        </p>
    </div>

    <div class="row {{ $eightoffset }}">
        <div class="{{ $alltwelve }}" v-for="object in etusivu.footer">
            <h1 class="{{ $allten }}" v-if="object.type == 'heading'">
                @{{ object.text }}
            </h1>
            <p class="{{ $allten }}" v-else>
                @{{ object.text }}
            </p>
            @if ( Auth::check() )
                <sc-edit-text class="{{ $alltwo }}"
                              :array.sync=etusivu.footer
                              :data.sync=object
                              :changes.sync=changes>
                </sc-edit-text>
            @endif
        </div>
        @if ( Auth::check() )
            <sc-add-text class="{{ $alltwelve }}"
                         :data.sync=etusivu.footer
                         :changes.sync=changes>
            </sc-add-text>
        @endif
    </div>
</div>
@endsection

@section('footer')
    <script src="/js/sc-etusivu.js"></script>

    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/fi_FI/sdk.js#xfbml=1&version=v2.5&appId=1580752288907010";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection
