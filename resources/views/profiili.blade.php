@extends('layouts.app')

@section('title')
Profiili
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hallintapaneeli</div>

                <div class="panel-body">
                    <h1>Hei, {{ Auth::user()->name }}!</h1>

                    <hr>

                    <h3>Vaihda salasana</h3>

                    @if ($virhe)
                        <div class="alert alert-danger">
                        <ul>
                        @foreach ($virhe as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                        </div>
                    @endif




                    <form method="post">
                        {{ csrf_field() }}
                        <!--<div class="form-group">
                            <label for="name">Nimi</label>
                            <input class="form-control"
                                   type="text"
                                   name="name"
                                   value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input class="form-control"
                                   type="email"
                                   name="email"
                                   value="{{ Auth::user()->email }}">
                        </div>-->
                        <div class="form-group">
                            <label for="old">Vanha salasana</label>
                            <input class="form-control"
                                   type="password"
                                   name="old">
                        </div>
                        <div class="form-group">
                            <label for="new">Uusi salasana</label>
                            <input class="form-control"
                                   type="password"
                                   name="new">
                        </div>
                        <div class="form-group">
                            <label for="repeat">Toista uusi salasana</label>
                            <input class="form-control"
                                   type="password"
                                   name="repeat">
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                    class="form-control btn btn-success"
                            >
                                Päivitä tietoja
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
