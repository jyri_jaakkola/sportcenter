<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChildHinnasto;

class ParentHinnasto extends Model
{
    protected $table = 'parent_hinnasto';
    protected $fillable = ['type', 'price'];
    protected $casts = [
        'price' => 'float'
    ];

    public function childs() {
        return $this->hasMany(ChildHinnasto::class);
    }

    public function addChild(ChildHinnasto $child)
    {
        return $this->childs()->save($child);
    }
}
