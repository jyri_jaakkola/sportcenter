<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SCAukioloajat extends Model
{
    protected $table = 'sc_aukioloajat';
    protected $fillable = ['day', 'from', 'to'];
}
