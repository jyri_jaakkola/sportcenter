<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ChildHinnasto extends Model
{
    protected $table = 'child_hinnasto';
    protected $fillable = ['type', 'price'];
    protected $casts = [
        'price' => 'float'
    ];

    public function getRequestData(Request $request) {
        $this->type = $request->input('type');
        $this->price = $request->input('price');
    }

    public function parent() {
        return $this->belongsTo(ParentHinnasto::class);
    }
}
