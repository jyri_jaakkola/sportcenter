<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SCAukioloajat;
use App\SCHinnasto;
use App\ParentHinnasto;
use App\ChildHinnasto;
use File;

class APIController extends Controller
{
    public function addAukiolo(Request $request, SCAukioloajat $aukiolo) {
        $aukiolo = $aukiolo->create($request->all());

        return $aukiolo;
    }

    public function addHinnasto(Request $request, SCHinnasto $hinnasto) {
        $hinnasto = $hinnasto->create($request->all());

        return $hinnasto;
    }

    public function updateAukiolo(Request $request, SCAukioloajat $aukiolo) {
        $aukiolo->update($request->all());

        return $aukiolo;
    }

    public function updateHinnasto(Request $request, SCHinnasto $hinnasto) {
        $hinnasto->update($request->all());

        return $hinnasto;
    }

    public function updateYhteystiedot(Request $request) {
        $data = json_encode($request->all());

        File::put('json/yhteystiedot.json', $data);

        return "{'success': true}";
    }

    public function updateEtusivu(Request $request) {
        $data = json_encode($request->all());

        File::put('json/sport-center.json', $data);

        return "{'success': true}";
    }

    public function updateAukioloJson(Request $request) {
        $data = json_encode($request->all());

        File::put('json/aukiolo-ja-hinnasto.json', $data);

        return "{'success': true}";
    }

    public function deleteAukiolo(Request $request, SCAukioloajat $aukiolo) {
        $aukiolo->delete();

        return $aukiolo;
    }

    public function deleteHinnasto(Request $request, SCHinnasto $hinnasto) {
        $hinnasto->delete();

        return $hinnasto;
    }


/**
 *  Hierontapalvelu
 */
    public function updateHPEtusivu(Request $request) {
        $data = json_encode($request->all());

        File::put('json/hierontapalvelu.json', $data);

        return "{'success': true}";
    }

    public function updatePalvelut(Request $request) {
        $data = json_encode($request->all());

        File::put('json/palvelut.json', $data);

        return "{'success': true}";
    }

    public function updateHPHinnasto(Request $request) {
        $data = json_encode($request->all());

        File::put('json/hinnasto.json', $data);

        return "{'success': true}";
    }

    public function updateHPYhteystiedot(Request $request) {
        $data = json_encode($request->all());

        File::put('json/hp-yhteystiedot.json', $data);

        return "{'success': true}";
    }

    public function addParent(Request $request, ParentHinnasto $hinnasto) {
        $hinnasto = new ParentHinnasto($request->all());
        if ($hinnasto->price == "") {
            $hinnasto->price = null;
        }
        $hinnasto->save();

        return $hinnasto;
    }

    public function addChild(Request $request, ParentHinnasto $parent) {
        $child = new ChildHinnasto;
        $child->getRequestData($request);
        $child = $parent->addChild($child);

        return $child;
    }

    public function updateHPParent(Request $request, ParentHinnasto $hinnasto) {
        $updated = new ParentHinnasto($request->all());
        if ($updated->price == "") {
            $updated->price = null;
        }
        $hinnasto->type = $updated->type;
        $hinnasto->price = $updated->price;

        $hinnasto->save();

        return $hinnasto;
    }

    public function updateHPChild(Request $request, ChildHinnasto $hinnasto) {
        $hinnasto->update($request->all());

        return $hinnasto;
    }

    public function deleteHPParent(Request $request, ParentHinnasto $hinnasto) {
        $hinnasto->delete();

        return $hinnasto;
    }

    public function deleteHPChild(Request $request, ChildHinnasto $hinnasto) {
        $hinnasto->delete();

        return $hinnasto;
    }
}
