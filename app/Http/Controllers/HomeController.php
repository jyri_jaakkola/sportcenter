<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $virhe = [];
        return view('profiili', compact('virhe'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $virhe = [];

        if (Auth::validate(array('email' => $user->email, 'password' => $request->input('old')))) {
            if (strlen($request->input('new')) < 6) {
                $virhe['short'] = "Uusi salasana on liian lyhyt. Salasanan pitää olla vähintään 6 merkkiä.";
            } else {
                if ($request->input('new') == $request->input('repeat')) {
                    $user->password = bcrypt($request->input('new'));
                    $user->save();
                } else {
                    // New password and repeat not the same
                    $virhe['repeat'] = "Uusi salasana ei ollut toistettu oikein.";
                }
            }
        } else {
            // Old password was not correct.
            $virhe['old'] = "Vanha salasana ei ollut oikein.";
        }

        return view('profiili', compact('virhe'));
    }
}
