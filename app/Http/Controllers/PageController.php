<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SCAukioloajat;
use App\SCHinnasto;
use App\ParentHinnasto;

class PageController extends Controller
{
    /**
     *  Sport Center
     */
    public function aukioloajat() {
        return view('sportcenter.aukioloajat');
    }

    public function yhteystiedot() {
        return view('sportcenter.yhteystiedot');
    }

    /**
     *  Hierontapalvelu
    **/
    public function hpHome() {
        return view('hierontapalvelu.home');
    }

    public function hinnasto() {
        return view('hierontapalvelu.hinnasto');
    }

    public function palvelut() {
        return view('hierontapalvelu.palvelut');
    }

    public function hpYhteystiedot() {
        return view('hierontapalvelu.yhteystiedot');
    }
}
