<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'sportcenter.jaakkola.dy.fi'], function() {
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('/aukioloajat-ja-hinnasto', 'PageController@aukioloajat');
    Route::get('/yhteystiedot', 'PageController@yhteystiedot');
});

Route::group(['domain' => 'hierontapalvelu.jaakkola.dy.fi'], function() {
    Route::get('/', 'PageController@hpHome');
    Route::get('/hinnasto', 'PageController@hinnasto');
    Route::get('/palvelut', 'PageController@palvelut');
    Route::get('/yhteystiedot', 'PageController@hpYhteystiedot');
});

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/koti', 'PageController@hpHome');
// Route::get('/aukioloajat-ja-hinnasto', 'PageController@aukioloajat');
// Route::get('/yhteystiedot', 'PageController@hpYhteystiedot');
// Route::get('/yhteys', 'PageController@yhteystiedot');
// Route::get('/hinnasto', 'PageController@hinnasto');
// Route::get('/palvelut', 'PageController@palvelut');


// API-routes to get information
Route::get('/api/sc-aukioloajat', function() {
    return App\SCAukioloajat::all()->sortBy('day');
});
Route::get('/api/sc-hinnasto', function() {
    return App\SCHinnasto::all()->sortBy('normal');
});
Route::get('/api/hp-hinnasto', function() {
    $parents = App\ParentHinnasto::all();
    foreach ($parents as $parent) {
        $parent->childs;
    }
    return $parents;
});


Route::group(['middleware' => 'web'], function () {
    //Route::auth();

    // Authentication Routes...
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('logout', 'Auth\AuthController@logout');

    // Registration Routes...
    //$this->get('register', 'Auth\AuthController@showRegistrationForm');
    //$this->post('register', 'Auth\AuthController@register');

    // Password Reset Routes...
    //$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    //$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    //$this->post('password/reset', 'Auth\PasswordController@reset');

    Route::get('/profiili', 'HomeController@index');
    Route::post('/profiili', 'HomeController@update');



    // API-routes to add information
    Route::post('/api/sc-add-aukiolo', 'APIController@addAukiolo');
    Route::post('/api/sc-add-hinnasto', 'APIController@addHinnasto');
    Route::post('/api/hp-add-hinnasto', 'APIController@addParent');
    Route::post('/api/hp-add-hinnasto/{parent}', 'APIController@addChild');
    // API-routes to update information
    Route::patch('api/sc-update-aukiolo/{aukiolo}', 'APIController@updateAukiolo');
    Route::patch('api/sc-update-hinnasto/{hinnasto}', 'APIController@updateHinnasto');
    Route::patch('api/hp-update-parent/{hinnasto}', 'APIController@updateHPParent');
    Route::patch('api/hp-update-child/{hinnasto}', 'APIController@updateHPChild');
    Route::post('/api/sc-save-yhteystiedot', 'APIController@updateYhteystiedot');
    Route::post('/api/sc-save-etusivu', 'APIController@updateEtusivu');
    Route::post('/api/sc-save-aukiolo', 'APIController@updateAukioloJson');
    Route::post('/api/hp-save-etusivu', 'APIController@updateHPEtusivu');
    Route::post('/api/hp-save-palvelut', 'APIController@updatePalvelut');
    Route::post('/api/hp-save-hinnasto', 'APIController@updateHPHinnasto');
    Route::post('/api/hp-save-yhteystiedot', 'APIController@updateHPYhteystiedot');
    // API-routes to delete information
    Route::delete('api/sc-delete-aukiolo/{aukiolo}', 'APIController@deleteAukiolo');
    Route::delete('api/sc-delete-hinnasto/{hinnasto}', 'APIController@deleteHinnasto');
    Route::delete('api/hp-delete-parent/{hinnasto}', 'APIController@deleteHPParent');
    Route::delete('api/hp-delete-child/{hinnasto}', 'APIController@deleteHPChild');
});
