<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SCHinnasto extends Model
{
    protected $table = 'sc_hinnasto';
    protected $fillable = ['type', 'normal', 'student'];
    protected $casts = [
        'normal' => 'float',
        'student' => 'float'
    ];
}
