var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        "font-awesome.min.css",
        "lato.css",
        "bootstrap.min.css"
    ], 'public/css/vendor.css');
    mix.sass('app.scss');
    mix.scripts([
        'vendor/jquery.min.js',
        'vendor/bootstrap.min.js',
        'vendor/vue.min.js',
        'vendor/vue-resource.min.js'
    ], 'public/js/vendor.js');
});
